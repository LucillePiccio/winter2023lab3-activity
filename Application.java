public class Application {
	public static void main (String[]args){
		//creating student1 object
		Student st1 = new Student();
		st1.getStudentID = 1944540;
		st1.getProgramName = "Computer Science";
		st1.getHoursOfStudy = 20;
		
		//creating student2 object
		Student st2 = new Student();
		st2.getStudentID = 1948552;
		st2.getProgramName = "Arts";
		st2.getHoursOfStudy = 21;
	
		//using instance methods incorrectly
		//Student.getStudentID();
		
		//using the instance methods correctly for st1
		st1.giveID();
		st1.tellProgramName();
		st1.getStudyingHours();
		
		//using the instance methods correctly for st2
		st2.giveID();
		st2.tellProgramName();
		st2.getStudyingHours();
		
		//creating Student array with instance methods
		Student[] section3 = new Student[3];
		section3[0] = st1;
		section3[1] = st2;
		//when printing, should include the field we want to display
		System.out.println(section3[0].getStudentID);
		System.out.println(section3[1].getStudentID); 
		
		//demonstrating how to directly put object into an array
		section3[2] = new Student();
		section3[2].getStudentID = 2234561;
		section3[2].getProgramName = "Social Studies";
		section3[2].getHoursOfStudy = 23;
		System.out.println(section3[2].getStudentID);
		System.out.println(section3[2].getProgramName);
		System.out.println(section3[2].getHoursOfStudy);
		
		//error
		//System.out.println(section3[0].getHoursOfStudy);
		//a NullPointerException error
		//System.out.println(section3[2].getProgramName);
	}
}