public class Student {
	public int getStudentID;
	public String getProgramName;
	public int getHoursOfStudy;
	
	public void giveID(){
		System.out.println("My student number is: " + getStudentID);
	}
	public void tellProgramName(){
		System.out.println("My program name is " + getProgramName);
	}
	public void getStudyingHours(){
		System.out.println("I have been studying for " + getHoursOfStudy + " hours.");
	}
}